# ProPresenter Utility

A tool I've used at my workplace.

## TrimNewlines.go:
I chose go so that I could make a simple binary for macos or windows and someone could just click it to fix the library.

The problem was any files or slides created in ProPresenter in *Windows* and then imported into the software on *MacOS*, resulted in extra newlines on every single slide.

Iterates over ProPresenter library of .pro6 files and strips RTF text of extra newlines.

These files are really just XML files. The text is base64 encoded RTF. 


## download_plan.rb & get_songs.rb

Downloads service plans from planningcenter.com and takes the confusing plan and prints a list of songs that are planned for each service.
